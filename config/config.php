<?php
return [
	"name"   => 'CRM',
	"hasSub" => true,
	"sub" => ['Customer','CustomerCare'],
	"class"  => \Plugins\CRM\Plugin::class,
	"routes" => __DIR__ . '/../routes/routes.php'
];